import React, { useEffect, useState } from 'react';
import Wrapper from './UI/wrapper'
import ExpenseForm from './components/expense-form'
import ExpenseList from './components/expense-list'
import './App.css';

const App = () => {
  const [expenseList, setExpenseList] = useState([])

  useEffect(() => {
    const storedExpenses = localStorage.getItem("expenses")
    if (storedExpenses != null) {
      setExpenseList(JSON.parse(storedExpenses))
    }
  }, [])

  const saveExpenseHandler = (expenseData) => {
    setExpenseList((prevExpenses) => {
      const updatedList = [expenseData, ...prevExpenses]
      localStorage.setItem("expenses", JSON.stringify(updatedList))
      // console.log(updatedList)
      return updatedList
    })
  }

  const deleteExpenseHandler = (expenseId) => {
    const foundIndex = expenseList.findIndex(expense => expense.id === expenseId)
    if(foundIndex > -1) {
      // console.log(foundIndex)
      expenseList.splice(foundIndex, 1)
      setExpenseList((prevList) => {
        localStorage.setItem("expenses", JSON.stringify(prevList))
        return [...prevList]
      })
    }
  }

  return (
    <Wrapper>
      <h1 className="header">Expense Tracker</h1>
      <ExpenseForm onSaveExpense={saveExpenseHandler} />
      <ExpenseList list={expenseList} onDeleteExpense={deleteExpenseHandler} />
    </Wrapper>
  );
}

export default App;