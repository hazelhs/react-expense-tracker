import React from 'react';

import './expense-list.css'
import ExpenseItem from './expense-item';

const ExpenseList = (props) => {

    const deletedExpense = (expenseId) => {
        // console.log(expenseId)
        props.onDeleteExpense(expenseId)
    }

    return (
        <div className="expenses">
            {props.list.length === 0 && <div className="expenses__notFound">No Expenses Found</div>}

            {props.list.map(expense => (
                <ExpenseItem key={expense.id} item={expense} onDeleteExpense={deletedExpense} />
            ))}
        </div>
    )
}

export default ExpenseList