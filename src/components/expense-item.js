import React from 'react'
import ExpenseDate from './expense-date'

import './expense-item.css'

const ExpenseItem = (props) => {

    const deleteExpenseHandler = (expenseId) => {
        // console.log(expenseId)
        props.onDeleteExpense(expenseId)
    }

    return (
        <div className="expense-item">
            <div className="expense-item__row">
                <ExpenseDate date={props.item.expenseDate} />
                <div>{props.item.title}</div>
            </div>

            <div className="expense-item__row">
                <div className="expense-item__amount">
                    $ {props.item.amount}
                </div>
                <div className="expense-item__action">
                    <button type="button" 
                        onClick={() => deleteExpenseHandler(props.item.id)}>
                        Delete
                    </button>
                </div>
            </div>
        </div>
    )
}

export default ExpenseItem