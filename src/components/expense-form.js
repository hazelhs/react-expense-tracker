import React, { useState } from 'react'

import './expense-form.css'

const ExpenseForm = (props) => {

    const [title, setTitle] = useState('')
    const [amount, setAmount] = useState('')
    const [expenseDate, setExpenseDate] = useState('')

    const titleChangeHandler = e => {
        setTitle(e.target.value)
    }

    const amountChangeHandler = e => {
        setAmount(e.target.value)
    }

    const dateChangeHandler = e => {
        // console.log(e.target.value)
        setExpenseDate(e.target.value)
    }

    const submitFormHandler = (e) => {
        e.preventDefault()

        const expenseData = {
            id: Math.random(),
            title: title,
            amount: Number.parseFloat(amount).toFixed(2),
            expenseDate: new Date(expenseDate)
        }
        props.onSaveExpense(expenseData)

        setTitle('')
        setAmount('')
        setExpenseDate('')
    }


    return (
        <form className="expense-form" onSubmit={submitFormHandler}>
            <div className="expense-form__controls">
                <div className="expense-form__control">
                    <label>Title</label>
                    <div>
                        <input type="text" placeholder="Title"
                            value={title} 
                            onChange={titleChangeHandler} />
                    </div>
                </div>

                <div className="expense-form__control">
                    <label>Amount</label>
                    <div>
                        <input type="number" placeholder="Amount" step="0.01"
                            value={amount}
                            onChange={amountChangeHandler} />
                    </div>
                </div>

                <div className="expense-form__control">
                    <label>Date</label>
                    <div>
                        <input type="date" placeholder="Date" 
                            value={expenseDate}
                            onChange={dateChangeHandler} />
                    </div>
                </div>
            </div>

            <div className="expense-form__actions">
                <button type="submit">
                    Add Expense
                </button>
            </div>
        </form>
    )
}

export default ExpenseForm